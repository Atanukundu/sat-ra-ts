echo "Reproducing results for Table I of our paper."

cd ./dReal-3.16.06.02-linux/bin/
echo " "
echo "Running unsafe instances of DReach."
python3 unsafedReach.py
cd /usr/app
python3 read-csv.py ./dReal-3.16.06.02-linux/bin/unsafe_result_dReach/unsafedReach.csv
cp ./dReal-3.16.06.02-linux/bin/unsafe_result_dReach/unsafedReach.csv ./unsafedReach.csv

cd ./XSpeed-plan/build/
echo " "
echo "Running unsafe instances of BFS-Reach."
python3 unsafe-xspeed-bfs.py
cd /usr/app
python3 read-csv.py ./XSpeed-plan/build/unsafe_result_BFS/unsafe-BFS-Reach.csv
cp ./XSpeed-plan/build/unsafe_result_BFS/unsafe-BFS-Reach.csv ./unsafe-BFS-Reach.csv

cd ./XSpeed-plan/build/
echo " "
echo "Running unsafe instances of SAT-Reach."
python3 unsafe-xspeed-sat.py
cd /usr/app
python3 read-csv.py ./XSpeed-plan/build/unsafe_result_SAT/unsafe-SAT-Reach.csv
cp ./XSpeed-plan/build/unsafe_result_SAT/unsafe-SAT-Reach.csv ./unsafe-SAT-Reach.csv

echo " "
echo "Plotting the graphs for unsafe instances of SAT-Reach."
cp ./XSpeed-plan/build/F_oscillator_32.plt ./XSpeed-plan/build/unsafe_result_SAT/F_oscillator_32/F_oscillator_32.plt
cp ./XSpeed-plan/build/Two_tank_U1.plt ./XSpeed-plan/build/unsafe_result_SAT/Two_tank_U1/Two_tank_U1.plt
cp ./XSpeed-plan/build/NAV_U2.plt ./XSpeed-plan/build/unsafe_result_SAT/NAV_U2/NAV_U2.plt
cp ./XSpeed-plan/build/NAV_U22.plt ./XSpeed-plan/build/unsafe_result_SAT/NAV_U22/NAV_U22.plt

cd /usr/app
cd ./XSpeed-plan/build/unsafe_result_SAT/NAV_U2/
echo " "
echo "For NAV_U2"
gnuplot NAV_U2.plt
cp ./NAV_U2.pdf /usr/app/NAV_U2.pdf

cd /usr/app
cd ./XSpeed-plan/build/unsafe_result_SAT/NAV_U22/
echo " "
echo "For NAV_U22"
gnuplot NAV_U22.plt
cp ./NAV_U22.pdf /usr/app/NAV_U22.pdf

cd /usr/app
cd ./XSpeed-plan/build/unsafe_result_SAT/F_oscillator_32/
echo " "
echo "For F_oscillator_32"
gnuplot F_oscillator_32.plt
cp ./F_oscillator_32.pdf /usr/app/F_oscillator_32.pdf

cd /usr/app
cd ./XSpeed-plan/build/unsafe_result_SAT/Two_tank_U1/
echo " "
echo "For Two_tank_U1"
gnuplot Two_tank_U1.plt
cp ./Two_tank_U1.pdf /usr/app/Two_tank_U1.pdf

echo " "
echo "Reproducing results for Table II of our paper."

cd /usr/app
cd ./dReal-3.16.06.02-linux/bin/
echo " "
echo "Running safe instances of DReach."
python3 safedReach.py
cd /usr/app
python3 read-csv.py ./dReal-3.16.06.02-linux/bin/safe_result_dReach/safedReach.csv
cp ./dReal-3.16.06.02-linux/bin/safe_result_dReach/safedReach.csv ./safedReach.csv

cd ./XSpeed-plan/build/
echo " "
echo "Running safe instances of BFS-Reach."
python3 safe-xspeed-bfs.py
cd /usr/app
python3 read-csv.py ./XSpeed-plan/build/safe_result_BFS/safe-BFS-Reach.csv
cp ./XSpeed-plan/build/safe_result_BFS/safe-BFS-Reach.csv ./safe-BFS-Reach.csv

cd ./XSpeed-plan/build/
echo " "
echo "Running safe instances of SAT-Reach."
python3 safe-xspeed-sat.py
cd /usr/app
python3 read-csv.py ./XSpeed-plan/build/safe_result_SAT/safe-SAT-Reach.csv
cp ./XSpeed-plan/build/safe_result_SAT/safe-SAT-Reach.csv ./safe-SAT-Reach.csv

echo " "
echo "Saving results as a tarball (results.tar) in /usr/app/"
tar -cf results.tar unsafedReach.csv unsafe-BFS-Reach.csv unsafe-SAT-Reach.csv safedReach.csv safe-BFS-Reach.csv safe-SAT-Reach.csv F_oscillator_32.pdf Two_tank_U1.pdf NAV_U2.pdf NAV_U22.pdf

echo " "
echo -n "Now the script will pause for an hour. "
echo "Open another terminal window and type 'docker ps' to get the container id for the running REP image."
echo -n "Then type 'docker cp <container_id>:/usr/app/results.tar ./'. "
echo "This will copy the results tarball to your local system."
echo "You can then either kill this process or wait for it to terminate."
sleep 1h
