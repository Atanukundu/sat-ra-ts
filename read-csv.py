import csv
import sys
from tabulate import tabulate

filename = sys.argv[1]
table = []
count = 0
with open(filename, 'r') as file:
	reader = csv.reader(file)
	for row in reader:
		if count == 0:
			header = row
		else:
			table.append(row)
		count += 1

print(tabulate(table, headers=header))