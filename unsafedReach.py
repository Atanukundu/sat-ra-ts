import os
import re
import string
import subprocess 
import signal
from subprocess import Popen, PIPE

example = []

#for platoon
example.append(['platoon','timeout 3600s runlim -s 4096 ./dReach -u 1 -l 0 testcases/platoon/unsafe/platoon_hybrid.drh --precision 0.001 --visualize'])

#for Oscillator
example.append(['Oscillator','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/Oscillator/unsafe/loc1/Oscillator.drh --precision 0.001 --visualize'])

#for F_oscillator
example.append(['F_oscillator_4','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/F_Osc_4/unsafe/loc1/f_osc_4.drh --precision 0.001 --visualize'])
example.append(['F_oscillator_8','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/F_Osc_8/unsafe/loc1/f_osc_8.drh --precision 0.001 --visualize'])
example.append(['F_oscillator_16','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/F_Osc_16/unsafe/loc1/f_osc_16.drh --precision 0.001 --visualize'])
example.append(['F_oscillator_32','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/F_Osc_32/unsafe/loc1/f_osc_32.drh --precision 0.001 --visualize'])
example.append(['F_oscillator_64','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/F_Osc_64/unsafe/loc1/f_osc_64.drh --precision 0.001 --visualize'])

#for two_tank
example.append(['Two_tank_U1','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/two_tank/unsafe/on_off/two_tank.drh --precision 0.001 --visualize'])

#for NAV_3_2
example.append(['NAV_U1','timeout 3600s runlim -s 4096 ./dReach -u 6 -l 0 testcases/NAV_3_2/unsafe/Loc7/NAV_3_2.drh --precision 0.001 --visualize'])
example.append(['NAV_U2','timeout 3600s runlim -s 4096  ./dReach -u 7 -l 0 testcases/NAV_3_2/unsafe/Loc4/NAV_3_2.drh --precision 0.001 --visualize'])

#for Nav3_inst1
example.append(['NAV_U3','timeout 3600s runlim -s 4096  ./dReach -u 3 -l 0 testcases/Nav3_inst1/unsafe/Loc6/Nav3_inst1.drh --precision 0.001 --visualize'])
example.append(['NAV_U4','timeout 3600s runlim -s 4096  ./dReach -u 2 -l 0 testcases/Nav3_inst1/unsafe/Loc4/Nav3_inst1.drh --precision 0.001 --visualize'])

#for NAV_3_3
example.append(['NAV_U5','timeout 3600s runlim -s 4096  ./dReach -u 6 -l 0 testcases/NAV_3_3/unsafe/L020/NAV_3_3.drh --precision 0.001 --visualize'])
example.append(['NAV_U6','timeout 3600s runlim -s 4096  ./dReach -u 5 -l 0 testcases/NAV_3_3/unsafe/L021/NAV_3_3.drh --precision 0.001 --visualize'])


#for NAV_3_4
example.append(['NAV_U7','timeout 3600s runlim -s 4096  ./dReach -u 1 -l 0 testcases/NAV_3_4/unsafe/L0100/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_U8','timeout 3600s runlim -s 4096  ./dReach -u 8 -l 0 testcases/NAV_3_4/unsafe/L1111/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_U9','timeout 3600s runlim -s 4096  ./dReach -u 6 -l 0 testcases/NAV_3_4/unsafe/L1212/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_U10','timeout 3600s runlim -s 4096  ./dReach -u 12 -l 0 testcases/NAV_3_4/unsafe/L2020/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_U11','timeout 3600s runlim -s 4096  ./dReach -u 9 -l 0 testcases/NAV_3_4/unsafe/L2111/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_U12','timeout 3600s runlim -s 4096  ./dReach -u 11 -l 0 testcases/NAV_3_4/unsafe/L2120/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_U13','timeout 3600s runlim -s 4096  ./dReach -u 10 -l 0 testcases/NAV_3_4/unsafe/L2121/NAV_3_4.drh --precision 0.001 --visualize'])


#for NAV30
example.append(['NAV_U14','timeout 3600s runlim -s 4096  ./dReach -u 2 -l 0 testcases/30/unsafe/loc_531/30.drh --precision 0.001 --visualize'])
example.append(['NAV_U15','timeout 3600s runlim -s 4096  ./dReach -u 12 -l 0 testcases/30/unsafe/loc_411/30.drh --precision 0.001 --visualize'])
example.append(['NAV_U16','timeout 3600s runlim -s 4096  ./dReach -u 9 -l 0 testcases/30/unsafe/loc_434/30.drh --precision 0.001 --visualize'])
example.append(['NAV_U17','timeout 3600s runlim -s 4096  ./dReach -u 10 -l 0 testcases/30/unsafe/loc_435/30.drh --precision 0.001 --visualize'])

#for NAV07
example.append(['NAV_U18','timeout 3600s runlim -s 4096  ./dReach -u 10 -l 0 testcases/nav07/unsafe/loc_554/nav07.drh --precision 0.001 --visualize'])
example.append(['NAV_U19','timeout 3600s runlim -s 4096  ./dReach -u 12 -l 0 testcases/nav07/unsafe/loc_528/nav07.drh --precision 0.001 --visualize'])
example.append(['NAV_U20','timeout 3600s runlim -s 4096  ./dReach -u 13 -l 0 testcases/nav07/unsafe/loc_503/nav07.drh --precision 0.001 --visualize'])
example.append(['NAV_U21','timeout 3600s runlim -s 4096  ./dReach -u 15 -l 0 testcases/nav07/unsafe/loc_453/nav07.drh --precision 0.001 --visualize'])
example.append(['NAV_U22','timeout 3600s runlim -s 4096  ./dReach -u 17 -l 0 testcases/nav07/unsafe/loc_403/nav07.drh --precision 0.001 --visualize'])
example.append(['NAV_U23','timeout 3600s runlim -s 4096  ./dReach -u 18 -l 0 testcases/nav07/unsafe/loc_378/nav07.drh --precision 0.001 --visualize'])

#for parallel edges of NAV_3_2
example.append(['NAV_U1_P','timeout 3600s runlim -s 4096 ./dReach -u 6 -l 0 testcases/NAV_3_2/unsafe/Loc7_P/NAV_3_2_P.drh --precision 0.001 --visualize'])





if not os.path.isdir('unsafe_result_dReach'):
	os.mkdir('unsafe_result_dReach')
f = open("unsafe_result_dReach/unsafedReach.csv", "w")
f.write("Benchmark, Bound, Time, SAT/UNSAT, #Paths\n")
mycwd = os.getcwd()
for i in range(0,len(example)):
	string_cmd = example[i][1]
	data = example[i][0].split()
	print('\nrunning '+ example[i][0])
	p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)
	output = str(p.communicate())
	#print(output)
	stat = output.find("-- SAT")
	if stat == -1:
		sat_unsat = "UNSAT"
	else:
		sat_unsat = "SAT"
	#print(sat_unsat)
	#print(ac_status)
	os.system('pids=$(pgrep dReal) && if [ ${#pids[@]} > 0 ]; then kill -9 $pids; fi')
	if (p.poll() == 0):
		time = "[runlim] real:"
		time_str = output.find(time)
		time_start = int(time_str)+len(time)
		time_end =  int(time_str)+len(time)+20
		real_time = output[time_start:time_end]
		real_time = re.sub("[^\d\.]", "", real_time)
		#print(real_time)
	# for retriving number of paths:
	str_directory = example[i][1].split()
	str_directory = str_directory[10].split("/")
	#print(str_directory)
	directory = str_directory[:-1]
	string = ""
	for dir in directory:
		string += dir + '/'
	#print(directory)
	#print(string)
	path, dirs, files = next(os.walk(string))
	paths = int((len(files)/3) - 1)
	#print(paths)
	os.chdir(mycwd)
	# for depth:
	bound = "For k ="
	bound_str = output.rfind(bound)
	bound_start = int(bound_str)+len(bound)+1
	bound_end = int(bound_str)+len(bound)+3
	ac_bound = output[bound_start:bound_end]
	ac_bound = re.sub(r'\,','', ac_bound)
	#print(ac_bound)
	if (p.poll() == 0):
		f.write(data[0] + ',' + ac_bound + ',' + real_time + ','+ sat_unsat + ',' + str(int(paths)) + '\n')
	else:
		if (p.poll() == 124):
			bound = int(ac_bound) + 1
			f.write(data[0] + ',' + str(bound) + ',' + 'Timeout ' + ','+ '-' + ',' + str(int(paths)) + '\n')
		elif (p.poll() == 3):
			f.write(data[0] + ',' + '-' + ',' + 'OOM ' + ','+ '-' + ',' + str(int(paths)) + '\n')


f.close()
#print("\nresult_dReach file created!")











