import os
import re
import string
import subprocess
from subprocess import Popen, PIPE

example = []

#for Two_tank
example.append(['Two_tank_S1' , 'timeout 3600s runlim -s 4096 ./dReach -u 12 -l 0 testcases/two_tank/safe/on_on/two_tank.drh --precision 0.001 --visualize'])

#for NAV_3_2
example.append(['NAV_S1' , 'timeout 3600s runlim -s 4096 ./dReach -u 10 -l 0 testcases/NAV_3_2/safe/Loc_9/NAV_3_2.drh --precision 0.001 --visualize'])

#for ACCS03
example.append(['ACCS03_1' , 'timeout 3600s runlim -s 4096 ./dReach -u 9 -l 0 testcases/ACCS03/safe/crs_crash_rcv/ACCS03.drh --precision 0.001 --visualize'])

example.append(['ACCS03_2' , 'timeout 3600s runlim -s 4096 ./dReach -u 9 -l 0 testcases/ACCS03/safe/crs_crash_crash/ACCS03.drh --precision 0.001 --visualize'])

#for NAV_3_3
example.append(['NAV_S2' , 'timeout 3600s runlim -s 4096 ./dReach -u 6 -l 0 testcases/NAV_3_3/safe/L022/NAV_3_3.drh --precision 0.001 --visualize'])
example.append(['NAV_S3' , 'timeout 3600s runlim -s 4096 ./dReach -u 6 -l 0 testcases/NAV_3_3/safe/L112/NAV_3_3.drh --precision 0.001 --visualize'])
example.append(['NAV_S4' , 'timeout 3600s runlim -s 4096 ./dReach -u 20 -l 0 testcases/NAV_3_3/safe/L211/NAV_3_3.drh --precision 0.001 --visualize'])
example.append(['NAV_S5' , 'timeout 3600s runlim -s 4096 ./dReach -u 20 -l 0 testcases/NAV_3_3/safe/L222/NAV_3_3.drh --precision 0.001 --visualize'])

#for NAV_3_4
example.append(['NAV_S6' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/NAV_3_4/safe/L0011/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_S7' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/NAV_3_4/safe/L0021/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_S8' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/NAV_3_4/safe/L0222/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_S9' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/NAV_3_4/safe/L1200/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_S10' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/NAV_3_4/safe/L2221/NAV_3_4.drh --precision 0.001 --visualize'])
example.append(['NAV_S11' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/NAV_3_4/safe/L2222/NAV_3_4.drh --precision 0.001 --visualize'])

#for ACCS05
example.append(['ACCS05_1' , 'timeout 3600s runlim -s 1024 ./dReach -u 4 -l 0 testcases/ACCS05/safe/crs_crs_crs_crash_crash/ACCS05.drh --precision 0.001 --visualize'])
example.append(['ACCS05_2' , 'timeout 3600s runlim -s 1024 ./dReach -u 4 -l 0 testcases/ACCS05/safe/crs_crs_rcv_crash_crs/ACCS05.drh --precision 0.001 --visualize'])

#for NAV_30
example.append(['NAV_S12' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/30/safe/loc_515/30.drh --precision 0.001 --visualize'])
example.append(['NAV_S13' , 'timeout 3600s runlim -s 4096 ./dReach -u 15 -l 0 testcases/30/safe/loc_540/30.drh --precision 0.001 --visualize'])
example.append(['NAV_S14' , 'timeout 3600s runlim -s 4096 ./dReach -u 20 -l 0 testcases/30/safe/loc_546/30.drh --precision 0.001 --visualize'])

#for NAV_07
example.append(['NAV_S15' , 'timeout 3600s runlim -s 4096 ./dReach -u 16 -l 0 testcases/nav07/safe/loc_430/nav07.drh --precision 0.001 --visualize'])
example.append(['NAV_S16' , 'timeout 3600s runlim -s 4096 ./dReach -u 10 -l 0 testcases/nav07/safe/loc_388/nav07.drh --precision 0.001 --visualize'])

if not os.path.isdir('safe_result_dReach'):
	os.mkdir('safe_result_dReach')
f = open("safe_result_dReach/safedReach.csv", "w")
f.write("Benchmark, Bound, Time, SAT/UNSAT, #Paths\n")
mycwd = os.getcwd()
for i in range(0,len(example)):
	string_cmd = example[i][1]
	data = example[i][0].split()
	print('\nrunning '+ example[i][0])
	p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)
	output = str(p.communicate())
	#print(output)
	stat = output.find("-- SAT")
	if stat == -1 and p.poll() == 0:
		sat_unsat = "UNSAT"
	elif p.poll() == 0:
		sat_unsat = "SAT"
	#print(sat_unsat)
	#print(p.poll())
	#print(ac_status)
	os.system('pids=$(pgrep dReal) && if [ ${#pids[@]} > 0 ]; then kill -9 $pids; fi')
	if (p.poll() == 0):
		time = "[runlim] real:"
		time_str = output.find(time)
		time_start = int(time_str)+len(time)
		time_end =  int(time_str)+len(time)+20
		real_time = output[time_start:time_end]
		real_time = re.sub("[^\d\.]", "", real_time)
		#print(real_time)
	# for retriving number of paths:
	str_directory = example[i][1].split()
	str_directory = str_directory[10].split("/")
	#print(str_directory)
	directory = str_directory[:-1]
	string = ""
	for dir in directory:
		string += dir + '/'
	#print(directory)
	#print(string)
	path, dirs, files = next(os.walk(string))
	paths = int((len(files)/3) - 1)
	#print(paths)
	os.chdir(mycwd)
	# for depth:
	bound = "For k ="
	bound_str = output.rfind(bound)
	bound_start = int(bound_str)+len(bound)+1
	bound_end = int(bound_str)+len(bound)+3
	ac_bound = output[bound_start:bound_end]
	ac_bound = re.sub(r'\,','', ac_bound)
	#print(ac_bound)
	if (p.poll() == 0):
		f.write(data[0] + ',' + ac_bound + ',' + real_time + ','+ sat_unsat + ',' + str(int(paths)) + '\n')
	else:
		if p.poll() == 124:
			bound = int(ac_bound) + 1
			f.write(data[0] + ',' + str(bound) + ',' + 'Timeout ' + ','+ '-' + ',' + str(int(paths)) + '\n')
		elif p.poll() == 3:
			f.write(data[0] + ',' + '-' + ',' + 'OOM ' + ','+ '-' + ',' + str(int(paths)) + '\n')


f.close()
#print("\ndReach.csv file created!")





