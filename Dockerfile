FROM ubuntu:18.04

# install Git, Wget and CMake

RUN	apt-get update && apt-get upgrade -y
RUN	apt-get install -y git
RUN apt install -y build-essential libssl-dev
RUN apt install -y wget
RUN apt install -y cmake
RUN apt install -y python3

# install openjdk16

RUN mkdir /usr/app
WORKDIR /usr/app
RUN wget https://download.java.net/java/GA/jdk16.0.1/7147401fd7354114ac51ef3e1328291f/9/GPL/openjdk-16.0.1_linux-x64_bin.tar.gz
RUN tar -xvf openjdk-16.0.1_linux-x64_bin.tar.gz
WORKDIR /usr/app/jdk-16.0.1
RUN update-alternatives --install /usr/bin/java java /usr/app/jdk-16.0.1/bin/java 1000
RUN update-alternatives --install /usr/bin/javac javac /usr/app/jdk-16.0.1/bin/javac 1000
RUN update-alternatives --config java
RUN update-alternatives --config javac

# install PPL

WORKDIR /usr/app
RUN wget https://www.bugseng.com/products/ppl/download/ftp/releases/1.2/ppl-1.2.tar.gz
RUN tar -xvf ppl-1.2.tar.gz
WORKDIR /usr/app/ppl-1.2
RUN apt install -y libgmp3-dev m4
RUN ./configure
RUN	make all && \
	make install

# install z3

WORKDIR /usr/app
RUN wget https://github.com/Z3Prover/z3/archive/refs/tags/z3-4.8.10.tar.gz
RUN tar -xvf z3-4.8.10.tar.gz
WORKDIR /usr/app/z3-z3-4.8.10/
RUN apt install -y python3-pip
RUN ls
RUN mkdir build && \
	cd build && \
	cmake -G "Unix Makefiles" ../ && \
	make -j4
WORKDIR /usr/app/z3-z3-4.8.10/build/
RUN make install

# install unittest-cpp

WORKDIR /usr/app
RUN git clone https://github.com/unittest-cpp/unittest-cpp
WORKDIR /usr/app/unittest-cpp/builds/
RUN cmake ../
RUN cmake --build ./
RUN cmake --build ./ --target install

# install GLPK

WORKDIR /usr/app
RUN wget https://ftp.gnu.org/gnu/glpk/glpk-4.35.tar.gz
RUN tar -xvf glpk-4.35.tar.gz
WORKDIR /usr/app/glpk-4.35
RUN ./configure && \
	make all && \
	make install

# install GMP

WORKDIR /usr/app
RUN wget https://gmplib.org/download/gmp/gmp-6.2.1.tar.lz
RUN apt install -y lzip
RUN tar -xvf gmp-6.2.1.tar.lz
WORKDIR /usr/app/gmp-6.2.1
RUN ./configure && \
	make all && \
	make install

# install NLOpt

WORKDIR /usr/app
RUN wget https://github.com/stevengj/nlopt/archive/v2.6.2.tar.gz
RUN tar -xvf v2.6.2.tar.gz
WORKDIR /usr/app/nlopt-2.6.2
RUN cmake . && make && make install

# install Boost

WORKDIR /usr/app
RUN wget https://sourceforge.net/projects/boost/files/boost/1.77.0/boost_1_77_0.tar.gz/download
RUN mv download boost_1_77_0.tar.gz
RUN tar -xvf boost_1_77_0.tar.gz
WORKDIR /usr/app/boost_1_77_0
RUN ./bootstrap.sh && \
	./b2 install

# install SUNDIALS

WORKDIR /usr/app
RUN wget https://github.com/LLNL/sundials/releases/download/v5.3.0/sundials-5.3.0.tar.gz
RUN tar -xvf sundials-5.3.0.tar.gz
WORKDIR /usr/app/sundials-5.3.0/
RUN mkdir instdir builddir
WORKDIR /usr/app/sundials-5.3.0/builddir/
RUN cmake .. && make && make install

# install GNUPlot

WORKDIR /usr/app
RUN apt install -y libcairo2-dev libpango1.0-dev
RUN wget https://sourceforge.net/projects/gnuplot/files/gnuplot/5.0.1/gnuplot-5.0.1.tar.gz/download
RUN mv download gnuplot-5.0.1.tar.gz
RUN tar -xvf gnuplot-5.0.1.tar.gz
WORKDIR /usr/app/gnuplot-5.0.1
RUN ./configure && \
	make && \
	make install
RUN pip3 install Popen PIPE tabulate
RUN apt install -y runlim libglpk40

# install Dreach

WORKDIR /usr/app
RUN wget https://github.com/dreal/dreal3/releases/download/v3.16.06.02/dReal-3.16.06.02-linux.tar.gz
RUN tar -xvf dReal-3.16.06.02-linux.tar.gz

# install XSpeed-plan

WORKDIR /usr/app
RUN git clone https://gitlab.com/Atanukundu/XSpeed-plan.git
WORKDIR /usr/app/XSpeed-plan/build
RUN make all
RUN apt install -y python

# Clone from SAT-RA-TS Repo

WORKDIR /usr/app
RUN git clone https://gitlab.com/Atanukundu/sat-ra-ts.git
RUN mv /usr/app/sat-ra-ts/read-csv.py /usr/app/
RUN mv /usr/app/sat-ra-ts/run_instances.sh /usr/app/
RUN mv /usr/app/sat-ra-ts/testcases /usr/app/dReal-3.16.06.02-linux/bin/
RUN mv /usr/app/sat-ra-ts/safedReach.py /usr/app/dReal-3.16.06.02-linux/bin/
RUN mv /usr/app/sat-ra-ts/unsafedReach.py /usr/app/dReal-3.16.06.02-linux/bin/

# After building XSpeed, repeat the results

WORKDIR /usr/app/
RUN chmod u+x ./run_instances.sh
CMD ./run_instances.sh